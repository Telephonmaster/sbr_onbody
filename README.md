## Wireless Insite SBR Simulatio Results for On-Body Scenarios

This repository contains post-processing scripts and simulation results for the IEEE ICC sork we submitted.

## System requirements

Python 3 with numpy/scipy
Octave with pythonic package installed

Since the scripts are cross-compatible between different systems, there should be no severe issue while running them. However, I recommend to use any Linux distro, since the environment is much easier to set up. Scripts are not optimized as of now, so performance may be an issue on weaker machines. I use a machine with i7-2600 cPU and 12 GB of RAM to run the scripts (with other tasks running in background).

## Overall workflow

The scripts load the data from the OLD folder, picking either Sitting or Standing scenario based on what is selected in the script.

total_filtration.m - used to build Fig. 8 from the paper, demonstrating how the antenna pattern affects received power.